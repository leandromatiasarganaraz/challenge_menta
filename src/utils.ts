import { NewDataStruct } from '/app/challenge_menta/src/types'


const parsetext = (textFromRequest: any): string => {
  if (!isString(textFromRequest)) {
    throw new Error('Incorrect or missing text')
  }

  return textFromRequest
}

const parseDate = (dateFromRequest: any): string => {
  if (!isString(dateFromRequest) || !isDate(dateFromRequest)) {
    throw new Error('Incorrect or missing date')
  }

  return dateFromRequest
}


const isString = (string: string): boolean => {
  return typeof string === 'string'
}

const isDate = (date: string): boolean => {
  return Boolean(Date.parse(date))
}


const NewEntry = (object: any): NewDataStruct => {
  const newEntry: NewDataStruct = {
    date: parseDate(object.date),
    text: parsetext(object.text)
  }

  return newEntry
}

export default NewEntry

 export function validate_circle(case1:string): any {
  const AceptSymbol: string[] = ["#","$","/","(",")","=","?","¿","_","-"];
  const One_Close: string[] = ["A","D","O","P","Q","R","a","b","d","e","g","o","p","q","@"];
  const Two_Close: string[] = ["%","∞","&","B"];
  const Three_Close: string[] = ["‰"];
  let cant = 0
  
  //let stringval = "valor que viene desde la funcionB"
  let result = case1.split('')

result.forEach((value: string) => {
  AceptSymbol.forEach((value2: string) => {
    console.log(value2)
    if(value==value2){
      cant =cant + 1
    }
  })
  One_Close.forEach((value2: string) => {
    console.log(value2)
    if(value==value2){
      cant =cant + 1
    }
  })
  Two_Close.forEach((value2: string) => {
    console.log(value2)
    if(value==value2){
      cant =cant + 2
    }
  })
  Three_Close.forEach((value2: string) => {
    console.log(value2)
    if(value==value2){
      cant =cant + 3
    }
  })
});

return cant
}