import express from 'express' //ES Modules

import * as ServicesMenta from '/app/challenge_menta/src/service/service'
import newEntry from '/app/challenge_menta/src/utils'

const router = express.Router()

router.get('/:id', (req, res) => {
    const response = ServicesMenta.findById(+req.params.id)
  
    return (response != null)
      ? res.send(response)
      : res.sendStatus(404)
  })

  router.get('/', (_req, res) => {
    res.send(ServicesMenta.getEntries())
  })

  router.post('/', (req, res) => {
    try {
      const newEntrypost = newEntry(req.body)
      console.log(req.body.text)
      const addEntry = ServicesMenta.addEntry(newEntrypost)
      res.json(addEntry)
    } catch (e) {
      res.status(400).send('no se pudo cargar')
    }
  })
export default router