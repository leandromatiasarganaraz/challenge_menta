export interface DataStruct {
    id: number
    date: string
    text: string
    cant: string
  }
  export type NewDataStruct = Omit<DataStruct, 'id' , 'cant' >
  
