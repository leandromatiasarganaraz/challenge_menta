import express from 'express'

import RoutesMenta from '/app/challenge_menta/src/routes/menta'

const app = express()
app.use(express.json()) //mw que transforma la req.body a un json

const PORT = 4500


app.use('/api/menta',RoutesMenta)


//prueba Api
//app.get('/ping',(_req, res) => {
//    res.send('pong')
//})

app.listen(PORT, () =>{
    console.log(`Server corriendo en puerto ${PORT}`)
})
