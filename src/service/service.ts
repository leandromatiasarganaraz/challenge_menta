import { DataStruct,NewDataStruct } from '/app/challenge_menta/src/types'

import data from '/app/challenge_menta/src/service/desafio.json'



const Data : NewDataStruct[] = data as NewDataStruct[]

export const getEntries = (): NewDataStruct[] => Data

export const findById = (id: number): NewDataStruct | undefined => {
    const entry = data.find(d => d.id === id)
    if (entry != null) {
    return entry
    }
  
    return undefined
  }

  export const addEntry = (newDataStruct: NewDataStruct): DataStruct=> {

    const newEntry = {
      id: Math.max(...Data.map(d => d.id)) + 1,
      cant:'' ,   
      ...newDataStruct
    }
    
    
    let cants = validate_circle(newEntry.text)
    newEntry.cant = cants
    Data.push(newEntry)
    return newEntry
  }

   function validate_circle(case1:string): any {
    const AceptSymbol: string[] = ["#","$","/","(",")","=","?","¿","_","-"];
    const One_Close: string[] = ["A","D","O","P","Q","R","a","b","d","e","g","o","p","q","@"];
    const Two_Close: string[] = ["%","∞","&","B"];
    const Three_Close: string[] = ["‰"];
    let cant = 0
    
    //let stringval = "valor que viene desde la funcionB"
    let result = case1.split('')
  
  result.forEach((value: string) => {
    AceptSymbol.forEach((value2: string) => {
      console.log(value2)
      if(value==value2){
        cant =cant + 1
      }
    })
    One_Close.forEach((value2: string) => {
      console.log(value2)
      if(value==value2){
        cant =cant + 1
      }
    })
    Two_Close.forEach((value2: string) => {
      console.log(value2)
      if(value==value2){
        cant =cant + 2
      }
    })
    Three_Close.forEach((value2: string) => {
      console.log(value2)
      if(value==value2){
        cant =cant + 3
      }
    })
  });
  
  return cant
  }